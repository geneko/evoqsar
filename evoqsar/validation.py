# This file contains all of the QSAR validation metrics
from __future__ import absolute_import, division, print_function, unicode_literals

from sklearn.metrics import mean_squared_error, accuracy_score, matthews_corrcoef
from scipy import stats


# Classification Metrics
def accuracy(y, yHat):
    """Computes the classification accuracy.

    :param y:
    :param yHat:
    :return:
    """
    accuracy = accuracy_score(y, yHat)

    return accuracy


def mcc(y, yHat):
    """Computes the matthews correlation coefficient of a classification model.

    :param y: Observed dependent values.
    :param yHat: Predicted dependent values.
    :return: mcc score
    """
    mcc = matthews_corrcoef(y, yHat)

    return mcc


# Regression Metrics
def mse(y, yHat):
    """Computes the mean squared error of a regression model.

    :param y:
    :param yHat:
    :return:
    """
    mse = mean_squared_error(y, yHat)

    return mse


def r2(y, yHat):
    """Compute the coefficient of determination.
    This equation can also be used to compute the cross-validated r^2 (q^2).
    
    Parameters
    ----------
    y : Observed dependent values.

    yHat : Predicted dependent values.
    
    """
    numer = ((y - yHat) ** 2).sum()  # Residual Sum of Squares
    denom = ((y - y.mean()) ** 2).sum()
    r2 = 1 - numer / denom

    return r2


def r2Pred(yTrain, yTest, yHatTest):
    """Compute the predicted r^2 of the test dataset.
    
    Parameters
    ----------
    yTrain : Observed dependent values of the training dataset.
    
    yTest : Observed dependent values of the test dataset.
    
    yHatTest : Predicted dependent values of the test dataset.
        
    """

    numer = ((yHatTest - yTest) ** 2).sum()
    denom = ((yTest - yTrain.mean()) ** 2).sum()
    r2Pred = 1 - numer / denom

    return r2Pred


# ######## Tropsha Metrics
# # Refer to Kunal Roy's papr for correct equations
def r2Test(y, yHat):
    """Compute the correlation coefficient of the test dataset.
    
    Parameters
    ----------
    y : Observed dependent values.
    
    yHat : Predicted dependent values.
        
    """
    r2Test = stats.pearsonr(y, yHat)[0] ** 2

    return r2Test


def k(y, yHat):
    """Compute slopes
    
    Parameters
    ----------
    y :
    
    yHat :
        
    """
    numer = (y * yHat).sum()
    denom = (yHat ** 2).sum()
    k = numer / denom

    denom = (y ** 2).sum()
    kP = numer / denom
    return k, kP


def r0(y, yHat, k, kP):
    """Compute correlation for regression lines through the origin
    
    Parameters
    ----------
    k : float
        Slope
    kP : float
        Slope
    
    """
    numer = ((y - k * yHat) ** 2).sum()
    denom = ((y - y.mean()) ** 2).sum()
    r2_0 = 1 - numer / denom

    numer = ((yHat - kP * y) ** 2).sum()
    denom = ((yHat - yHat.mean()) ** 2).sum()
    rP2_0 = 1 - numer / denom
    return r2_0, rP2_0