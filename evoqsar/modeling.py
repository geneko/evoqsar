# This file contains all of the modeling methods available to use.
#
# All modeling methods must have at a minimum the following functions in the same manner as scikit-learn. This is to
# ensure consistency between all of the various methods:
# model.fit(x, y): Trains a model using the dependent matrix x (NxM) and independent vector y (Nx1).
# y = model.predict(x): Fits the dependent matrix x (NxM) to the trained model and outputs the predicted independent
# vector y (Nx1).
#
# The function init() is designed such that the modelType parameter is the same as the model method function to be used.
# This is to prevent excessive if/else statements to execute the function and is achieved using Python's globals()
# function.
#
from __future__ import absolute_import, division, print_function, unicode_literals

from sklearn import linear_model  # Multiple linear regression, logistic regression
from sklearn import cross_decomposition  # Partial least squares regression
from sklearn import ensemble  # Extremely randomized trees and Random Forest
from sklearn import discriminant_analysis  # Linear/Quadratic discriminant analysis


# This function is not designed to be altered. Add the appropriate wrapper to the various modeling methods below this
# function.
def init(obj, modelMethod, *args, **kwargs):
    """Initializes the modeling method with the appropriate arguments
    
    Parameters
    ----------
    obj : object
        Modeling object
    
    modelMethod : {'MLR', 'PLSR', 'RFregress', 'ERTregress',
                    'RFclassify, ERTclassify, 'LDA', 'QDA', 'LR'}
        The modeling method used to derive a model. The following methods are available:

        Regression
        ----------
        MLR (multiple linear regression)
        PLSR (partial least squares regression)
        RFregress (Random Forest)
        ERTregress (extremely randomized trees)


        Classification
        --------------
        LDA (linear discriminant analysis)
        QDA (quadratic discriminant analysis)
        LR (logistic regression)
        RFclassify (Random Forest)
        ERTclassify (Extremely randomized trees)


    Returns
    -------
    obj : Modeling object
    
    
    Other parameters
    ----------------
    All other parameters are passed directly to the model method. Refer to the appropriate documentation for parameters
    to be passed. The following parameters have been changed from their defaults.
    
    PLS : partial least squares
        scale=False
    
    """

    try:
        obj = globals()[modelMethod](obj, *args, **kwargs)
    except:
        print(modelMethod + ' is not a valid modeling method.')


# Model methods
def MLR(obj, *args, **kwargs):
    """Initializes the multiple linear regression modeling method
    See sklearn.linear_model.LinearRegression() for parameters.
    
    Parameters
    ----------
    obj : object
        Modeling object
          
    Returns
    -------
    obj : object
        Modeling object
    
    """
    obj.method_ = 'MLR'
    obj.type_ = 'regress'
    obj.model = linear_model.LinearRegression(*args, **kwargs)


def PLSR(obj, scale=False, *args, **kwargs):
    """Initializes the partial least squares regression modeling method
    See sklearn.cross_decomposition.PLSRegression() for parameters.
    
    Parameters
    ----------
    obj : object
        Modeling object
        
    scale : True/False
        This parameter has been changed from the default value. As the dataset is expected to already be standardized or
        normalized, this prevents the function from performing any additional scaling.
          
    Returns
    -------
    obj : object
        QSAR object
    
    """
    obj.method_ = 'PLSR'
    obj.type_ = 'regress'
    obj.model = cross_decomposition.PLSRegression(scale=False, *args, **kwargs)


def ERTregress(obj, *args, **kwargs):
    """Initializes the extremely randomized trees regressor modeling method
    See sklearn.ensemble.ExtraTreesRegressor() for parameters.
    
    Parameters
    ----------
    obj : object
        Modeling object
          
    Returns
    -------
    obj : object
        Modeling object
    
    """

    obj.method_ = 'ERTregress'
    obj.type_ = 'regress'
    obj.model = ensemble.ExtraTreesRegressor(*args, **kwargs)


def RFregress(obj, *args, **kwargs):
    """Initializes the random forest regressor modeling method
    See sklearn.ensemble.RandomForestRegressor() for parameters
    
    Parameters
    ----------
    obj : object
        Modeling object
          
    Returns
    -------
    obj : object
        Modeling object
    
    """

    obj.method_ = 'RFregress'
    obj.type_ = 'regress'
    obj.model = ensemble.RandomForestRegressor(*args, **kwargs)


def ERTclassify(obj, *args, **kwargs):
    """Initializes the extremely randomized trees classifier modeling method
    See sklearn.ensemble.ExtraTreesClassifier() for parameters

    Parameters
    ----------
    obj : object
        Modeling object

    Returns
    -------
    obj : object
        Modeling object

    """

    obj.method_ = 'ERTclassify'
    obj.type_ = 'classify'
    obj.model = ensemble.ExtraTreesClassifier(*args, **kwargs)


def RFclassify(obj, *args, **kwargs):
    """Initializes the random forest classifier modeling method
    See sklearn.ensemble.RandomForestClassifier() for parameters

    Parameters
    ----------
    obj : object
        Modeling object

    Returns
    -------
    obj : object
        Modeling object

    """

    obj.method_ = 'RFclassify'
    obj.type_ = 'classify'
    obj.model = ensemble.RandomForestClassifier(*args, **kwargs)


def LDA(obj, *args, **kwargs):
    """Initializes the linear discriminant analysis modeling method
    See sklearn.lda.LDA() for parameters

    Parameters
    ----------
    obj : object
        Modeling object

    Returns
    -------
    obj : object
        Modeling object

    """

    obj.method_ = 'LDA'
    obj.type_ = 'classify'
    obj.model = discriminant_analysis.LinearDiscriminantAnalysis(*args, **kwargs)


def QDA(obj, *args, **kwargs):
    """Initializes the quadratic discriminant analysis modeling method
    See sklearn.qda.QDA() for parameters.

    Parameters
    ----------
    obj : object
        Modeling object

    Returns
    -------
    obj : object
        Modeling object

    """

    obj.method_ = 'QDA'
    obj.type_ = 'classify'
    obj.model = discriminant_analysis.QuadraticDiscriminantAnalysis(*args, **kwargs)


def LR(obj, *args, **kwargs):
    """Initializes the logistic regression modeling method
    See sklearn.linear_model.LogisticRegression() for parameters.

    Parameters
    ----------
    obj : object
        Modeling object

    Returns
    -------
    obj : object
        Modeling object

    """

    obj.method_ = 'LR'
    obj.type_ = 'classify'
    obj.model = linear_model.LogisticRegression(*args, **kwargs)
