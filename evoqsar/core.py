from __future__ import absolute_import, division, print_function, unicode_literals

import ast
import copy
import time
import multiprocessing as mp
import platform
import re

import numpy
import psutil
import scipy

from sklearn import cross_validation

from . import modeling
from . import validation

def parallelFitness(idx):
    global pool

    # Compute fitness [parallel]
    fitnessMetrics_all = pool.map(computeFitness, idx)

    return fitnessMetrics_all

def computeFitness(idx):
    #define values
    global g_model
    global g_trainX
    global g_trainY
    global g_testX
    global g_testY
    global g_paramCV
    global g_paramFitness
    global g_trackFitness

    #re-define values
    model = g_model
    trainX = g_trainX
    trainY = g_trainY
    testX = g_testX
    testY = g_testY
    paramCV = g_paramCV
    paramFitness = g_paramFitness
    trackFitness = g_trackFitness

    metrics = {}  # Store fitness and statistical metrics

    # Scenarios where it is pointless to compute the fitness function.
    # DNM = Do No Model
    flagDNM = 0
    if len(idx) < 2: # Compute fitness if number of features is between 2+ and pMax
        flagDNM = 1
    elif len(idx) > paramFitness['pMax']:
        flagDNM = 1
    elif model.method_ == 'PLSR':  # In PLSR, number of features must be equal or greater than # of components.
        nComp = model.model.get_params()['n_components']
        if len(idx) < nComp:
            flagDNM = 1

    if flagDNM == 1:
        fitness = float('-inf')
    else:
        idxKey = str(idx)
        if idxKey in trackFitness:  # Check to see if feature subset have been tested.
            fitness = float('-inf')
        else:  # Compute fitness for selected model.
            trainX = trainX[:, idx]
            testX = testX[:, idx]

            try:
                model.train(trainX, trainY)
                model.test(testX, testY)
                model.cv(nFold=paramCV['nFold'], shuffle=paramCV['shuffle'], nRuns=paramCV['nRuns'])

                # Generalized cross-validation criteria as fitness function.
                if model.type_ == 'regress':
                    mse_cv = numpy.mean(model.mse_cv_)
                    mse_test = model.mse_test_

                    c = paramFitness['c']  # Penalty factor
                    p = trainX.shape[1]  # Number of features
                    n_train = trainX.shape[0]
                    n_test = testX.shape[0]
                    n = n_train + n_test  # Number of samples

                    sse_cv = mse_cv*n_train
                    sse_test = mse_test*n_test

                    numer = (sse_cv + sse_test)/n # MSE of (cv + test)
                    denom = (1 - (p * (c / n)))**2
                    gcv = numer/denom

                    fitness = 1/gcv
                    if fitness == float('inf'):  # Discard models which have perfect fit
                        fitness = float('-inf')

                # Modified generalized cross-validation (Substitute mean squared error with classification error)
                elif model.type_ == 'classify':
                    accuracy_cv = numpy.mean(model.accuracy_cv_)
                    accuracy_test = model.accuracy_test_
                    error = (1 - accuracy_cv) + (1 - accuracy_test)

                    c = paramFitness['c']  # Penalty factor
                    p = trainX.shape[1]  # Number of features
                    n = trainX.shape[0] + testX.shape[0]  # Number of samples

                    numer = error
                    denom = (1 - (p * (c / n))) ** 2
                    gcv_mod = numer/denom

                    fitness = 1/gcv_mod
                    if fitness == float('inf'):  # Discard models which have perfect fit
                        fitness = float('-inf')

                # Store model metrics for dataset
                if model.type_ == 'regress':
                    metrics['trackR2'] = model.r2_
                    metrics['trackR2Pred'] = model.r2Pred_
                    metrics['trackQ2'] = numpy.mean(model.q2_)
                    metrics['trackMSE_train'] = model.mse_train_
                    metrics['trackMSE_cv'] = numpy.mean(model.mse_cv_)
                    metrics['trackMSE_test'] = model.mse_test_
                elif model.type_ == 'classify':
                    metrics['trackMCC_train'] = model.mcc_
                    metrics['trackMCC_cv'] = numpy.mean(model.mcc_cv_)
                    metrics['trackMCC_test'] = model.mcc_test_
                    metrics['trackAccuracy_train'] = model.accuracy_
                    metrics['trackAccuracy_cv'] = numpy.mean(model.accuracy_cv_)
                    metrics['trackAccuracy_test'] = model.accuracy_test_

            except numpy.linalg.linalg.LinAlgError:  # Catch singular matrix errors in PLSR
                fitness = float('-inf')

    metrics['fitness'] = fitness
    metrics['idx'] = idx

    return metrics


class Model:
    def __init__(self, modelMethod, *args, **kwargs):
        """Modeling object

        Stores the type of learning (regression or classification) and the modeling method. This determines the type of
        validation statistics to compute.

        Parameters:
        -----------
        modelMethod: <string>
            * See modeling.py for valid values.

        """

        modeling.init(self, modelMethod, *args, **kwargs)

    # Train model with training set and compute statistics
    def train(self, trainX, trainY):
        """ Train model and compute predicted dependent values using a training set.

        Parameters:
        -----------
        trainX: <NumPy array>, shape=(nSamples, nFeatures)
            * Independent variables in the training set

        trainY: <NumPy array>, shape=(nSamples,)
            * Dependent variable in the training set

        Attributes (Regression):
        ------------------------
        trainYHat_: <NumPy array>, shape=(nSamples,)
            * Predicted dependent variables.

        r2_: <float>
            * R^2 statistic of the training set.

        r2Pred_: <float>
            * Predicted R^2 statistic (R^2_{pred}) of the test set.

        mse_train_: <float>
            * Mean squared error of the training set.

        Attributes (Classification):
        ----------------------------
        accuracy_: <float>
            * Percent accuracy of the training set.

        mcc_: <float>
            * Matthews correlation coefficient of the training set.
        """

        self.trainX_ = trainX
        self.trainY_ = numpy.ravel(trainY)

        self.model.fit(self.trainX_, self.trainY_)  # Train/fit model from sklearn defined in modeling.py
        self.trainYHat_ = self.predict(self.trainX_)

        # Compute statistics for a regression model
        if self.type_ == 'regress':
            # Compute basic statistics
            self.r2_ = validation.r2(self.trainY_, self.trainYHat_)
            self.mse_train_ = validation.mse(self.trainY_, self.trainYHat_)

        # Compute statistics for a classification model.
        elif self.type_ == 'classify':
            self.accuracy_ = validation.accuracy(self.trainY_, self.trainYHat_)
            self.mcc_ = validation.mcc(self.trainY_, self.trainYHat_)   # Matthews Correlation Coefficient

    # Predict test set and compute statistics
    def test(self, testX, testY):
        """ Test model and compute predicted dependent values using test set.

        Parameters:
        -----------
        testX: <NumPy Array>, shape=(nSamples, nFeatures)
            * Independent variables in the training set

        testY: <NumPy Array>, shape=(nSamples,)
            * Dependent variable in the test set

        Attributes (Regression):
        ------------------------
        testYHat_: <NumPy Array>, shape=(nSaples,)
            * Predicted dependent variables

        r2Pred_: <float>
            * Predicted R^2 statistic (R^2_{pred}) of the test set.

        mse_test_: <float>
            * Mean squared error of the test set.

        tropsha_: <dictionary>
            * Tropsha metrics

        Attributes (Classification):
        ----------------------------
        accuracy_test_: <float>
            * Percent accuracy of the test set.

        mcc_test_: <float>
            * Matthews correlation coefficient of the test set.
        """

        self.testX_ = testX
        self.testY_ = numpy.ravel(testY)

        # Get predicted values of test set
        self.testYHat_ = self.predict(self.testX_)

        # Compute statistics for a regression model
        if self.type_ == 'regress':
            # Compute basic statistics
            self.r2Pred_ = validation.r2Pred(self.trainY_, self.testY_, self.testYHat_)
            self.mse_test_ = validation.mse(self.testY_, self.testYHat_)

            # Metrics for computing Tropsha statistics
            self.r2Test_ = validation.r2Test(self.testY_, self.testYHat_)
            self.k_, self.kP_ = validation.k(self.testY_, self.testYHat_)  # k, k'
            self.r2_0_, self.rP2_0_ = validation.r0(self.testY_, self.testYHat_, self.k_, self.kP_)  # r^2_0, r^2'_0
            self.tropsha_ = {}
            self.tropsha_['r^2'] = self.r2Test_
            self.tropsha_['k'] = self.k_
            self.tropsha_['k\''] = self.kP_
            self.tropsha_['r^2_0'] = self.r2_0_
            self.tropsha_['r\'^2_0'] = self.rP2_0_
            self.tropsha_['(r^2 - r^2_0)/r^2'] = (self.r2Test_ - self.r2_0_)/self.r2Test_
            self.tropsha_['(r^2 - r\'^2_0)/r^2'] = (self.r2Test_ - self.rP2_0_)/self.r2Test_
            self.tropsha_['|r^2_0 - r\'^2_0|'] = numpy.abs(self.r2_0_ - self.rP2_0_)

        # Compute statistics for a classification model.
        elif self.type_ == 'classify':
            self.accuracy_test_ = validation.accuracy(self.testY_, self.testYHat_)
            self.mcc_test_ = validation.mcc(self.testY_, self.testYHat_)

    def predict(self, *args, **kwargs):
        """Model prediction function

        This function points to self.model.predict(). If function follows scikit-learn API, the following parameters
        are valid:

        Parameters:
        -----------
        X: <NumPy array>, shape=(nSamples, nFeatures)
            * Independent variables of dataset to be predicted.

        Returns:
        --------
        Y: <NumPy array>, shape=(nSamples)
            * Predicted dependent values of dataset to be predicted.

        """

        return numpy.ravel(self.model.predict(*args, **kwargs))    # Flatten into a 1D array.


    def predict_proba(self, *args, **kwargs):
        """Classification modeling function to predict the probability of each class prediction.

        This function points to self.model.predict_proba(). If function follows scikit-learn API, the following
        parameters are valid:

        Parameters:
        -----------
        X: <NumPy array>, shape=(nSamples, nFeatures)
            * Independent variables of dataset to be predicted.

        Returns:
        --------
        Y: <NumPy array>, shape=(nSamples, nClasses)
            * Predicted probabilities of each class of dataset to be predicted. Class order is defined in
                self.model.classes_

        """


        if self.type_ == 'classify':
            try:
                return self.model.predict_proba(self, *args, **kwargs)
            except:
                return None
        else:
            return None

    def cv(self, nFold=10, shuffle=True, nRuns=10):
        """Cross-validation function.

        This function performs cross-validation on regression models and stratified cross-validation on
        classification models.

        Parameters:
        -----------
        nFold: <int>, [0, nSamples]
            * Number of cross-validation folds to perform. If nFold < 2 or >= nSamples, leave-one-out is performed.

        shuffle: <True, False>
            * Shuffle dataset for each n-Fold cross-validation.

        nRuns: <int>
            * For n-Fold cross-validation, number of cross-validated runs to perform.

        Attributes (Regression):
        ------------------------
        q2_: <float> (leave-one-out) or <Numpy Array> (n-Fold)
            * Cross-validation statistic

        mse_cv_: <float> (leave-one-out) or <NumPy array> (n-Fold)
            * Cross-validation mean squared error

        Attributes (Classification):
        ----------------------------
        accuracy_cv_: <float> (leave-one-out) or <NumPy array> (n-Fold)
            * Cross-validated accuracy measure

        mcc_cv_: <float> (leave-one-out) or <NumPy array> (n-Fold)
            * Cross-validated Matthews correlation coefficient

        """

        modelCV = copy.deepcopy(self.model)
        yHatCV = numpy.zeros_like(self.trainY_)

        # Leave-one-out cross-validation
        if nFold is None or nFold < 2 or nFold >= self.trainX_.shape[0]:
            # Use SKL cross validation indexing engine to create splits.
            idx = cross_validation.LeaveOneOut(self.trainY_.shape[0])

            for trainIdx, testIdx in idx:
                modelCV.fit(self.trainX_[trainIdx, :], self.trainY_[trainIdx])
                yHatCV[testIdx] = modelCV.predict(self.trainX_[testIdx, :])

            if self.type_ == 'regress':
                self.q2_ = validation.r2(self.trainY_, yHatCV)
                self.mse_cv_ = validation.mse(self.trainY_, yHatCV)
            elif self.type_ == 'classify':
                self.accuracy_cv_ = validation.accuracy(self.trainY_, yHatCV)
                self.mcc_cv_ = validation.mcc(self.trainY_, yHatCV)

        # K-Fold cross-validation
        else:
            if nRuns > 1:
                if self.type_ == 'regress':
                    self.q2_ = numpy.zeros(nRuns)
                    self.mse_cv_ = numpy.zeros(nRuns)
                elif self.type_ == 'classify':
                    self.accuracy_cv_ = numpy.zeros(nRuns)
                    self.mcc_cv_ = numpy.zeros(nRuns)

            for i in range(nRuns):
                # Regression: KFold CV; Classification: Stratified KFold CV
                if self.type_ == 'regress':
                    idx = cross_validation.KFold(n=self.trainY_.shape[0], n_folds=nFold, shuffle=shuffle)
                elif self.type_ == 'classify':
                    idx = cross_validation.StratifiedKFold(y=self.trainY_, n_folds=nFold, shuffle=shuffle)

                for trainIdx, testIdx in idx:
                    modelCV.fit(self.trainX_[trainIdx, :], self.trainY_[trainIdx])
                    yHatCV[testIdx] = modelCV.predict(self.trainX_[testIdx, :])

                if self.type_ == 'regress':
                    self.q2_[i] = validation.r2(self.trainY_, yHatCV)
                    self.mse_cv_[i] = validation.mse(self.trainY_, yHatCV)
                elif self.type_ == 'classify':
                    self.accuracy_cv_[i] = validation.accuracy(self.trainY_, yHatCV)
                    self.mcc_cv_[i] = validation.mcc(self.trainY_, yHatCV)


    def yRand(self, nRand=100):
        """y-Randomization compares R^2 and LOO-CV Q^2 in regression or MCC in classification.

        Parameters:
        -----------
        nRand: <int>, [1, inf)

        Attributes (Regression):
        ------------------------
        r2_yRand_: <NumPy array>, shape=(nRand)
            * R^2 statistic for each randomized run.

        q2_yRand_: <NumPy array>, shape=(nRand)
            * Q^2 statistic (leave-one-out) for each randomized run.

        Attributes (Classification):
        ----------------------------
        mcc_yRand_: <NumPy array>, shape=(nRand)
            * Matthews correlation coefficient for each randomized run.

        mcc_cv_yRand_: <NumPy array>, shape=(nRand)
            * Cross-validated Matthews correlation coefficient for each randomized run.

        """

        if self.type_ == 'regress':
            self.r2_yRand_ = numpy.zeros(nRand)
            self.q2_yRand_ = numpy.zeros(nRand)
        elif self.type_ == 'classify':
            self.mcc_yRand_ = numpy.zeros(nRand)
            self.mcc_cv_yRand_ = numpy.zeros(nRand)

        modelRand = copy.deepcopy(self.model)

        for i in range(0, nRand):
            yRand = numpy.random.permutation(self.trainY_)  # Permute observations

            # Fit Model
            modelRand.fit(self.trainX_, yRand)
            yHatRand = numpy.ravel(modelRand.predict(self.trainX_))

            # Leave-one-out Cross-Validation
            yHatCV = numpy.zeros_like(self.trainY_)
            idx = cross_validation.LeaveOneOut(self.trainY_.shape[0])
            for trainIdx, testIdx in idx:
                modelRand.fit(self.trainX_[trainIdx, :], yRand[trainIdx])
                yHatCV[testIdx] = numpy.ravel(modelRand.predict(self.trainX_[testIdx, :]))

            if self.type_ == 'regress':
                self.r2_yRand_[i] = validation.r2(self.trainY_, yHatRand)
                self.q2_yRand_[i] = validation.r2(self.trainY_, yHatCV)
            elif self.type_ == 'classify':
                self.mcc_yRand_[i] = validation.mcc(self.trainY_, yHatRand)
                self.mcc_cv_yRand_[i] = validation.mcc(self.trainY_, yHatCV)

    def leverage(self, xQuery=None):
        """Calculates the leverage of training or query sample.
        Parameters:
        -----------
        xTrain : <NumPy array>, shape=(nSamples, nFeatures)
            * Data matrix used to compute the model.

        xQuery : <NumPy array>, shape=(nSamples, nFeatures)
            * Data matrix to compute the leverage.
            If None, compute the leverage of the training data xTrain.

        Returns:
        --------
        h: <NumPy array>, shape=(nSamples)
            * Returns NumPy array of the leverage of the query sample.

        hWarn: <float>
            * Returns value of warning leverage.
            For a training set, h > hWarn may be highly influential data points.
            For prediction, h > hWarn indicates the predicted value is outside the model domain of applicability and
            requires extrapolation to make a prediction.

            hWarn = 3*(p+1)/n, p=number of features, n=number of samples in training set.
        """

        if xQuery is None:  # If no query sample, compute the leverage of training set.
            xQuery = self.trainX_

        hWarn = 3*(self.trainX_.shape[1] + 1)/self.trainX_.shape[0]    # Warning leverage

        trainInv = numpy.linalg.inv(self.trainX_.T.dot(self.trainX_))

        h = []
        for i in range(0, xQuery.shape[0]):
            h.append(xQuery[i, :].dot(trainInv).dot(xQuery[i, :].T))

        return numpy.array(h), hWarn

class EA:
    def __init__(self, model, trainX, trainY, validX, validY, paramCV={'nFold': 10, 'shuffle': True, 'nRuns': 10},
                 popSize=40, maxGen=1000, maxModels=None, initFeat=3, paramFitness={'c': 2}, parallel=1):
        """Evolutionary algorithm object for feature selection.

        Parameters:
        -----------
        model: <object>
            * Modeling object from Model() class in core.py

        trainX: <NumPy array>, shape=(nSamples, nFeatures)

        trainY: <NumPy array>, shape=(nSamples,)

        validX: <NumPy array>, shape=(nSamples, nFeatures)

        validY: <NumPy array>, shape=(nSamples,)

        paramCV: <dictionary>, keys={'nFold', 'shuffle', 'nRuns'}
            * Dictionary of cross-validation parameters. See Model.cv() for parameters.

        popSize: <int>, [1, inf)
            * Population size for evolutionary algorithm feature selection search.

        maxGen: <int>, [1, inf)
            * Stopping criteria. Maximum number of generations to run.

        maxModels: <int>, [1, inf)
            * Stopping criteria. Stop searching after the maximum number of predictive models have been discovered.

        initFeat: <int>, [1, nFeatures]
            * Initialize population with an average number of 'initFeat' selected by each individual.

        paramFitness: <dictionary>, keys={'c': <float>, (0, inf)}
            * Dictionary of fitness function parameters. 'c' is the penalty factor with values ranging from (0, inf).
                c=2 is default.

        parallel: <int>, [-1, nCPUs]
            * Parallelize evolutionary algorithm feature selection. Currently, only Linux and OS X is parallelizable.
                -1 indicates parallelization with all available processors
                0 indicates no parallelization
                [1, nCPUs] indicates run with this many workers.

        """


        # Store dataset
        self.trainX_ = trainX
        self.trainY_ = numpy.ravel(trainY)  # Convert to 1D vector
        self.testX_ = validX
        self.testY_ = numpy.ravel(validY)

        ######################################################
        # Define Global Variables
        global g_model
        global g_trainX
        global g_trainY
        global g_testX
        global g_testY
        global g_paramCV
        global g_paramFitness
        global g_trackFitness

        # Determine OS. If Windows, run serial. If not windows, run based on the following:
        # 'parallel'
        #    if 'parallel' is equal to 0, run with 1 processor
        #    if 'parallel' is less than 0, or greater than number of available processors, run all available processors
        #    else run user specified 'parallel' processes
        global myOS
        global nCPUs
        global pool
        myOS = platform.system()
        #nCPUs = psutil.cpu_count(logical=False)  # Maximum number of physical cores.
        nCPUs = psutil.cpu_count(logical=True)  # Gets the number of logical cores of the system.
                                                # Total number of true physical cores cannot be determined on Linux.

        if re.match('Windows', myOS):
            nCPUs = 1
        else:
            if parallel == 0:
                nCPUs = 1
            elif parallel < 0 or parallel > nCPUs:
                nCPUs = nCPUs
            else:
                nCPUs = parallel
        self.nCPUs_ = nCPUs
        print ('OS: '+myOS)
        print ('Running code with '+str(nCPUs)+' processor')
        ####################################################


        # Check dimensionality of arrays.
        if self.trainX_.shape[1] != self.testX_.shape[1]:
            raise ValueError('Dimensionality mismatch of training and test sets.')
        if self.trainX_.shape[0] != self.trainY_.shape[0] or self.testX_.shape[0] != self.testY_.shape[0]:
            raise ValueError('Sample size mismatch between dependent and independent variable arrays.')

        self.model = model  # Modeling object (see modeling.py)
        self.popSize_ = popSize  # Population size
        self.dimSize_ = self.trainX_.shape[1]  # Dimension size

        # Stopping Criteria
        self.maxGen_ = maxGen  # Maximum number of generations
        self.maxModels_ = maxModels # Maximum number of predictive models

        # The desired number/percentage of features to be selected is determined by initFeat.
        #
        # It is generally agreed that between 5 and six observations are required per descriptor in order to minimize
        # chance of overfitting.
        # C.L. Waller, "A Comparative QSAR Study Using CoMFA, HQSAR, and FRED/SKEYS Paradigms for Estrogen Receptor
        # Binding Affinities of Structurally Diverse Compounds", J. Chem. Inf. Comput. Sci. 2004, 44, 758-465
        #
        # There should be a minimum of a 5:1 ratio of experimental data to descriptors.
        # M. Krein, T.-W. Huang, L. Morkowchuk, D.K. Agrafiotis, C.M. Breneman, "Developing Best Practices for
        # Descriptor-Based Property Prediction: Appropriate Matching of Datasets, Descriptors, Methods, and
        # Expectations" in Statistical Modelling of Molecular Descriptors in QSAR/QSPR, 2012
        #
        # Topliss, J. G.; Edwards, R. P. Chance Factors in Studies of Quantitative Structure-Activity Relationships.
        # J. Med. Chem. 1979, 22, 1238-1244.
        if initFeat is None or initFeat <= 0 or initFeat > self.dimSize_:
            initFeat = self.trainX_.shape[0] / 5  # Set initial ratio 1 feature per 5 observations
        if initFeat < 1:
            self.eps_ = initFeat
        else:
            self.eps_ = initFeat / self.dimSize_

        # Cross-validation options
        # self.cv_ = cv    # True/False
        self.paramCV_ = paramCV
        nFold = self.paramCV_['nFold']
        if nFold is None or nFold < 2 or nFold >= self.trainX_.shape[0]:  # Leave-one-out cross-validation
            self.paramCV_['shuffle'] = None
            self.paramCV_['nRuns'] = None
        #self.cv_nFold_ = cv_nFold    # Number of cross-validation folds. None indicates leave-one-out.
        #self.cv_shuffle_ = cv_shuffle    # Shuffle training set for each cross-validation run.
        #self.cv_nRuns_ = cv_nRuns    # Number of cross-validation runs.

        # Fitness Function Parameters
        self.paramFitness_ = paramFitness
        # Determine maximum number of features allowed.
        c = self.paramFitness_['c']
        n = self.trainY_.shape[0] + self.testY_.shape[0]
        pMax = round(scipy.optimize.newton(func=lambda x: (1 - x*c/n)**2, x0=0), 2)
        self.paramFitness_['pMax'] = pMax

        # Track fitness and model metrics
        # By keeping track, we can prevent the re-calculation of the model should the feature be re-selected in
        # subsequent generations
        self.trackFitness_ = {}
        self.trackGlobal_ = []  # Track global fitness (benchmarking)
        self.trackPredModelCount_ = []  # Track number of predictive models (benchmarking)
        self.trackPredModel_ = {}  # Keep track of predictive models
        if self.model.type_ == 'regress':
            self.trackR2_ = {}
            self.trackR2Pred_ = {}
            self.trackQ2_ = {}
            self.trackMSE_train_ = {}
            self.trackMSE_cv_ = {}
            self.trackMSE_test_ = {}
        elif self.model.type_ == 'classify':
            self.trackMCC_train_ = {}
            self.trackMCC_cv_ = {}
            self.trackMCC_test_ = {}
            self.trackAccuracy_train_ = {}
            self.trackAccuracy_cv_ = {}
            self.trackAccuracy_test_ = {}

        # Redefine Global Values
        g_model = model
        g_trainX = self.trainX_
        g_trainY = self.trainY_
        g_testX = self.testX_
        g_testY = self.testY_
        g_paramCV = paramCV
        g_paramFitness = paramFitness
        g_trackFitness = self.trackFitness_

    # Store model statistical metrics
    def processMetrics(self, idxKey, metrics):
        self.trackFitness_[idxKey] = metrics['fitness']
        if self.model.type_ == 'regress':
            self.trackR2_[idxKey] = metrics['trackR2']
            self.trackR2Pred_[idxKey] = metrics['trackR2Pred']
            self.trackQ2_[idxKey] = metrics['trackQ2']
            self.trackMSE_train_[idxKey] = metrics['trackMSE_train']
            self.trackMSE_cv_[idxKey] = metrics['trackMSE_cv']
            self.trackMSE_test_[idxKey] = metrics['trackMSE_test']
            # Check if model is predictive
            if self.trackR2_[idxKey] > 0.6 and self.trackQ2_[idxKey] > 0.5 and self.trackR2Pred_[idxKey] > 0.5:
                self.trackPredModel_[idxKey] = metrics['fitness']
        elif self.model.type_ == 'classify':
            self.trackFitness_[idxKey] = metrics['fitness']
            self.trackMCC_train_[idxKey] = metrics['trackMCC_train']
            self.trackMCC_cv_[idxKey] = metrics['trackMCC_cv']
            self.trackMCC_test_[idxKey] = metrics['trackMCC_test']
            self.trackAccuracy_train_[idxKey] = metrics['trackAccuracy_train']
            self.trackAccuracy_cv_[idxKey] = metrics['trackAccuracy_cv']
            self.trackAccuracy_test_[idxKey] = metrics['trackAccuracy_test']
            # Check if model is predictive
            if self.trackMCC_train_[idxKey] > 0.4 and self.trackMCC_cv_[idxKey] > 0.4 and \
                            self.trackMCC_test_[idxKey] > 0.4:
                self.trackPredModel_[idxKey] = metrics['fitness']

        # Define Global Variable
        global g_trackFitness
        g_trackFitness = self.trackFitness_

    # Stopping Criterion Check. This function also prints output on the screen.
    def stopCriteriaCheck(self, verbose):
        timePoint = time.time()

        breakFlag = 0

        # Check if predictive models stopping criteria reached
        if self.maxModels_:
            if len(self.trackPredModel_) >= self.maxModels_:
                output = str(self.gen_) + '/' + str(self.maxGen_) + \
                    ', Elapsed Time: ' + str((timePoint-self.timeStart_)/60) + \
                    ', Fitness: ' + str(self.fitnessBestGlobal_) + \
                    ', Predictive Models: ' + str(len(self.trackPredModel_))
                print(output)
                print('Stopping criteria reached.')
                breakFlag = 1

        # Output statistics to screen
        if verbose and breakFlag == 0:
            if self.gen_ == 1 or self.gen_ % 10 == 0 or self.gen_ == self.maxGen_:
                output = str(self.gen_) + '/' + str(self.maxGen_) + \
                    ', Elapsed Time: ' + str((timePoint-self.timeStart_)/60) + \
                    ', Fitness: ' + str(self.fitnessBestGlobal_) + \
                    ', Predictive Models: ' + str(len(self.trackPredModel_))
                print(output)

        return breakFlag

    # Initialize population and velocity for DE-BPSO and BPSO
    def swarmInit(self, alpha, beta, random):
        self.gen_ = 0  # Set generation number

        self.alpha_ = numpy.linspace(alpha[0], alpha[1], self.maxGen_)

        # Adjust bit flip mutation (beta) to a percentage if beta>=1
        if beta >= 1:
            beta = beta / self.dimSize_
        self.beta_ = beta

        # Adjust number of particles to perform random search
        if random < 1:
            self.random_ = int(round(random * self.popSize_))
        elif random > self.popSize_:  # Set to 10% of particles to randomly search
            self.random_ = int(round(0.1 * self.popSize_))
        else:
            self.random_ = int(round(random))

        self.position_ = numpy.zeros((self.popSize_, self.dimSize_), dtype=int)  # Current positions of the population
        self.velocity_ = numpy.random.rand(self.popSize_, self.dimSize_)  # Initialize velocity
        self.positionBestLocal_ = numpy.zeros((self.popSize_, self.dimSize_), dtype=int)  # Store local best positions
        self.positionBestGlobal_ = numpy.zeros(self.dimSize_, dtype=int)  # Store global best position
        self.fitnessBestLocal_ = numpy.zeros(self.popSize_)  # Store fitness of local best positions
        self.fitnessBestGlobal_ = 0  # Store fitness of global best position

        # Initialize population
        self.position_[self.velocity_ <= self.eps_] = 1
        self.positionBestLocal_ = self.position_.copy()  # Initialize best positions with initial population

        #############################################################################################################
        # Compute fitness of population [If Windows, serial]
        if self.nCPUs_ == 1:  # Serial
            for i in range(0, self.popSize_):
                idx = self.position_[i].nonzero()[0].tolist()
                fitnessMetrics = computeFitness(idx)
                fitness = fitnessMetrics['fitness']
                self.fitnessBestLocal_[i] = fitness
                if fitness > float('-inf'):
                    idxKey = str(idx)  # Convert index to a dictionary hash key
                    self.processMetrics(idxKey, fitnessMetrics)
        else:  # Parallel
            idx = []
            for i in range(0, self.position_.shape[0]):
                idx.append(self.position_[i].nonzero()[0].tolist())
            fitnessMetrics_all = parallelFitness(idx)
            for i, fitnessMetrics in enumerate(fitnessMetrics_all):
                fitness = fitnessMetrics['fitness']
                self.fitnessBestLocal_[i] = fitness
                if fitness > float('-inf'):
                    idxKey = str(fitnessMetrics['idx'])  # Convert index to a dictionary hash key
                    self.processMetrics(idxKey, fitnessMetrics)
        #############################################################################################################

        # Store fitness of current population (For elitism)
        self.fitnessLocal_ = self.fitnessBestLocal_.copy()

        # Determine the global best fitness and position
        self.fitnessBestGlobal_ = self.fitnessBestLocal_.max()
        self.positionBestGlobal_ = self.positionBestLocal_[self.fitnessBestLocal_.argmax()]
        self.velocityBest_ = self.velocity_[self.fitnessBestLocal_.argmax()]    # Store best velocity for elitism

    # Update the positions for the next generation
    def swarmUpdate(self):
        # Translate velocity vector to binary bits
        alpha = self.alpha_[self.gen_]
        beta = self.beta_

        # # Update position based on BPSO rules
        idxLocal = numpy.logical_and(alpha < self.velocity_, self.velocity_ <= 0.5 * (1 + alpha))
        idxGlobal = 0.5 * (1 + alpha) < self.velocity_
        self.position_[idxLocal] = self.positionBestLocal_[idxLocal]
        self.position_[idxGlobal] = self.positionBestGlobal_.reshape(-1, self.dimSize_).repeat(self.popSize_, axis=0)[
            idxGlobal]

        # Apply bit mutation
        rnd = numpy.random.rand(self.popSize_, self.dimSize_)
        idxMut = rnd <= beta
        self.position_[idxMut] = 1 - self.position_[idxMut]

        # Select particle in a random manner to perform random search
        if self.random_ > 0:
            randIdx = numpy.random.permutation(self.popSize_)[0:self.random_]  # Particle index to perform random search

            rnd = numpy.random.rand(self.random_, self.dimSize_)
            rndPop = numpy.zeros_like(rnd, dtype=int)
            rndPop[rnd < self.eps_] = 1
            self.position_[randIdx, :] = rndPop

    # Compute fitness of swarm population and update local and global best fitness
    def swarmCompute(self):
        #############################################################################################################
        # Compute fitness of new population [If Windows = serial]
        if self.nCPUs_ == 1:  # Serial
            for i in range(0, self.popSize_):
                idx = self.position_[i].nonzero()[0].tolist()
                fitnessMetrics = computeFitness(idx)
                fitness = fitnessMetrics['fitness']
                self.fitnessLocal_[i] = fitness # Store local fitness for elitism
                if fitness > self.fitnessBestLocal_[i]:
                    self.positionBestLocal_[i] = self.position_[i]
                    self.fitnessBestLocal_[i] = fitness
                if fitness > float('-inf'):
                    idxKey = str(idx) # Convert index to a dictionary hash key
                    self.processMetrics(idxKey, fitnessMetrics)
        else:  # Parallel
            idx = []
            for i in range(0, self.position_.shape[0]):
                idx.append(self.position_[i].nonzero()[0].tolist())
            fitnessMetrics_all = parallelFitness(idx)
            for i, fitnessMetrics in enumerate(fitnessMetrics_all):
                fitness = fitnessMetrics['fitness']
                self.fitnessLocal_[i] = fitness # Store local fitness for elitism
                if fitness > self.fitnessBestLocal_[i]:
                    self.positionBestLocal_[i] = self.position_[i]
                    self.fitnessBestLocal_[i] = fitness
                if fitness > float('-inf'):
                    idxKey = str(fitnessMetrics['idx']) # Convert index to a dictionary hash key
                    self.processMetrics(idxKey, fitnessMetrics)
        #############################################################################################################

        # Update the global best fitness and position
        if self.fitnessBestGlobal_ < self.fitnessBestLocal_.max():
            self.fitnessBestGlobal_ = self.fitnessBestLocal_.max()
            self.positionBestGlobal_ = self.positionBestLocal_[self.fitnessBestLocal_.argmax()]
            self.velocityBest_ = self.velocity_[self.fitnessBestLocal_.argmax()]

        # Keep track of global fitness over the generations (Useful for benchmarking)
        self.trackGlobal_.append(self.fitnessBestGlobal_)
        self.trackPredModelCount_.append(len(self.trackPredModel_))

    def debpso(self, f=0.8, cr=0.9, alpha=(0.5, 0.33), beta=1.5, random=0, elitism=None, verbose=True):
        """Differential Evolution-Binary Particle Swarm Optimization

        Parameters:
        -----------
        f: <float>, (0, 2]
            * Scaling factor

        cr: <cr>, (0, 1)
            * Crossover rate

        alpha: <tuple>, (alphaMin, alphaMax), alphaMin <= alphaMin (0, 1)
            *

        beta: <float>, (0, inf)
            * bit-flip rate. If beta >= 1, this is the average number of bits to flip during each generation update. The
                bit-fip rate is computed by beta/nFeatures. Otherwise bit-flip rate=beta if beta<1

        verbose: <True, False>
            * Print generation, elapsed time, global optimal fitness, number of predictive models every 10 generations.
        """
        # Activate multiprocessing processes
        global pool
        global nCPUs
        if nCPUs > 1:
            pool = mp.Pool(nCPUs)

        self.f_ = f  # Scaling factor
        self.cr_ = cr  # Crossover rate

        self.timeStart_ = time.time()

        # Initialize swarm
        self.swarmInit(alpha, beta, random)

        for gen in range(0, self.maxGen_):
            # Differential-Evolution (de/rand/1)
            # DE here is used to create trial velocity vectors.
            # Create trial velocity vector
            mutation = numpy.zeros_like(self.velocity_)
            velocityPrev = self.velocity_.copy()  # Store previous velocity for elitism

            # Create trial velocity vector (mutation)
            for i in range(0, self.popSize_):
                if self.f_ == 0:  # Dither
                    f = 0.5*(1 + numpy.random.rand())
                else:
                    f = self.f_

                # Select 3 random vectors distinct from i
                idx = numpy.random.permutation(range(0, self.popSize_))[0:3]
                while i in idx:
                    idx = numpy.random.permutation(range(0, self.popSize_))[0:3]
                mutation[i] = self.velocity_[idx[0]] + f * (self.velocity_[idx[1]] - self.velocity_[idx[2]])

            # Mutate velocity bits with trial vector (mutation) bits if crossover rate (cr) satisfied
            random = numpy.random.rand(self.popSize_, self.dimSize_)
            self.velocity_[random < cr] = mutation[random < cr]
            idx = numpy.tile(range(0, self.dimSize_), [self.popSize_, 1])
            randIdx = numpy.random.randint(0, self.dimSize_, size=(self.popSize_, 1))
            self.velocity_[idx == randIdx] = mutation[idx == randIdx]

            # Rescale velocity to [0, 1]
            vMin = self.velocity_.min(axis=1).reshape((self.popSize_, 1))
            vMax = self.velocity_.max(axis=1).reshape((self.popSize_, 1))
            self.velocity_ = (self.velocity_ - vMin) / (vMax - vMin)

            # Update population according to the rules of BPSO
            self.swarmUpdate()

            # Compute the fitness of new population
            self.swarmCompute()

            if elitism is not None:
                if elitism == 'weak':   # Only carry over velocity of the global best.
                    idx = self.fitnessBestLocal_ == self.fitnessBestGlobal_
                    self.velocity_[idx] = self.velocityBest_
                elif elitism == 'strong':  # Keep new velocity if fitness is better in all particles
                    idx = self.fitnessLocal_ < self.fitnessBestLocal_
                    self.velocity_[idx] = velocityPrev[idx]

            # Increment generation number
            self.gen_ += 1

            # Check stopping criteria
            breakFlag = self.stopCriteriaCheck(verbose)
            if breakFlag == 1:
                break

        # Cleanly shutdown multiprocessing processes
        if nCPUs > 1:
            pool.close()
            pool.join()

    def bpso(self, alpha=(0.5, 0.33), beta=1.5, random=0, verbose=True):
        """Binary Particle Swarm Optimization

        Parameters:
        -----------
        alpha: <tuple>, (alphaMin, alphaMax), alphaMin <= alphaMin (0, 1)
            *

        beta: <float>, (0, inf)
            * bit-flip rate. If beta >= 1, this is the average number of bits to flip during each generation update. The
                bit-fip rate is computed by beta/nFeatures. Otherwise bit-flip rate=beta if beta<1

        verbose: <True, False>
            * Print generation, elapsed time, global optimal fitness, number of predictive models every 10 generations.
        """
        global pool
        global nCPUs
        if nCPUs > 1:
            pool = mp.Pool(nCPUs)

        self.timeStart_ = time.time()

        # Initialize swarm
        self.swarmInit(alpha, beta, random)

        for gen in range(0, self.maxGen_):
            # Randomize velocity
            self.velocity_ = numpy.random.rand(self.popSize_, self.dimSize_)

            # Update population according to the rules of BPSO
            self.swarmUpdate()

            # Compute the fitness of new population
            self.swarmCompute()

            # Increment generation number
            self.gen_ += 1

            breakFlag = self.stopCriteriaCheck(verbose)
            if breakFlag == 1:
                break

        # Cleanly shutdown multiprocessing processes
        if nCPUs > 1:
            pool.close()
            pool.join()


    def toFile(self, filename):
        """Write predictive models to a comma-separated values (csv) file.

        Parameters:
        ------------
        filename: <string>

        """

        with open(filename, 'w') as f:
            if self.model.type_ == 'regress':
                header = 'Features,Fitness,R2,Q2,R2Pred\n'
            elif self.model.type_ == 'classify':
                header = 'Features,Fitness,MCC_train,MCC_cv,MCC_test,Accuracy_train,Accuracy_cv,Accuracy_test\n'
            f.write(header)

            for key in self.trackPredModel_.keys():
                idx = ast.literal_eval(key)  # Convert key string to list
                idx = str([int(idxNum) for idxNum in idx])  # Strip "L" suffix and convert to string
                if self.model.type_ == 'regress':
                    output = '"' + idx + '",' + str(self.trackFitness_[key]) + ',' + \
                             str(self.trackR2_[key]) + ',' + str(self.trackQ2_[key]) + ',' + \
                             str(self.trackR2Pred_[key]) + '\n'
                    f.write(output)

                elif self.model.type_ == 'classify':
                    output = '"' + idx + '",' + str(self.trackFitness_[key]) + ',' + \
                             str(self.trackMCC_train_[key]) + ',' + str(self.trackMCC_cv_[key]) + ',' + \
                             str(self.trackMCC_test_[key]) + ',' + \
                             str(self.trackAccuracy_train_[key]) + ',' + str(self.trackAccuracy_cv_[key]) + ',' + \
                             str(self.trackAccuracy_test_[key]) + '\n'
                    f.write(output)

    def importance(self, filename=None, names=None):
        """Compute feature importance by counting the frequency of selection in predictive models.

        Parameters:
        -----------
        filename: <string>, optional

        names: <list>, optional
            * List of feature names.

        Returns:
        --------
        featFreq: <list: (idxNum, idxCount)>
            * Returns a list containing tuples of the index ID and the frequency of selection.
        """

        idxFreq = {}
        for idxKey in self.trackPredModel_:
            idx = ast.literal_eval(idxKey)  # Convert string
            idx = [int(idxNum) for idxNum in idx]  # Strip "L" suffix
            if self.model.type_ == 'regress':
                for idxNum in idx:
                    idxFreq[str(idxNum)] = idxFreq.get(str(idxNum), 0) + 1

            elif self.model.type_ == 'classify':
                for idxNum in idx:
                    idxFreq[str(idxNum)] = idxFreq.get(str(idxNum), 0) + 1

        if len(idxFreq) > 0:
            idxNum = []
            idxCount = []
            for idx in idxFreq:
                idxNum.append(int(idx))
                idxCount.append(idxFreq[idx])

            # Sort feature frequency and store as a tuple into a list (featFreq)
            order = numpy.argsort(idxCount)[::-1]
            featFreq = []
            for i in order:
                featFreq.append((idxNum[i], idxCount[i]))

            if filename is not None:
                if names is not None:
                    with open(filename, 'w') as f:
                        header = 'FeatureID,Feature,Frequency\n'
                        f.write(header)
                        for freq in featFreq:
                            idx = freq[0]
                            count = freq[1]
                            output = str(idx) + ',"' + str(names[idx]) + '",' + str(count) + '\n'
                            f.write(output)

                else:
                    with open(filename, 'w') as f:
                        header = 'FeatureID,Frequency\n'
                        f.write(header)
                        for freq in featFreq:
                            idx = freq[0]
                            count = freq[1]
                            output = str(idx) + ',' + str(count) + '\n'
                            f.write(output)

            return featFreq
        else:
            print('No predictive models.')
            return None