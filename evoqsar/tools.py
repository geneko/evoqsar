from __future__ import absolute_import, division, print_function, unicode_literals

import numpy
from scipy.spatial.distance import pdist, squareform

def kennardstone(data, k=None):
    """Kennard-Stone Algorithm
    This function process a data matrix and returns the index order according to the Kennard-Stone algorithm.

    Parameters:
    -----------
    data : <NumPy array>, shape=(nSamples, nFeatures)
        * Data matrix to compute the distances between samples according to the Kennard-Stone algorithm.

    k : <int>
        * Number of sample points.

    Returns:
    --------
    ksIdx: <list>
        * Returns index order of samples according to Kennard-Stone algorithm.

    """

    dataIdx = range(0, data.shape[0])   # Index of samples in dataset
    ksIdx = []  # Keep track of sample index for Kennard-Stone order
    
    if k is None or k < 2 or k > len(dataIdx):
        k = len(dataIdx)

    # First, we need to determine the two furthest samples
    # Compute Pairwise Euclidean Distance
    dist = squareform(pdist(data, 'euclidean'))
    
    # Get index of the two furthest samples
    maxDist = dist.max()
    for i, j in enumerate(dist.argmax(axis=0)):
        if dist[i, j] == maxDist:
            break
    # Store kennard-stone index of the two samples and delete from data index
    ksIdx.append(i)
    ksIdx.append(j)
    dataIdx.remove(i)
    dataIdx.remove(j)
    
    ksData = data[[i, j],:]   # Initalize Kennard-Stone Matrix with first two compounds
    data = numpy.delete(data, [i, j], axis=0) # delete first two compounds from data matrix
    
    # Find subsequent samples
    if k > len(ksIdx):
        for i in range(0, data.shape[0] - 1):
            matrix = numpy.append(ksData, data, axis=0)
            dist = squareform(pdist(matrix), 'euclidean')
            dist = dist[0:ksData.shape[0], ksData.shape[0]:]    # Extract distance matrix between KS and remaining samples
            
            # Find maximum dissimilarity (Observation with maximum minimum distance)        
            minIdx = dist.argmin(axis=1)    # Get sample index of minimum distance to KS samples
            maxIdx = dist.min(axis=1).argmax()
            obsIdx = minIdx[maxIdx]
            
            # Append observation to Kennard-Stone matrix and delete from data matrix
            ksData = numpy.append(ksData, data[obsIdx, :].reshape(1, data.shape[1]), axis=0)
            data = numpy.delete(data, obsIdx, axis=0)
            ksIdx.append(dataIdx.pop(obsIdx))   # Store index of sample point.
    
            if k == len(ksIdx):
                break
    
    # Get index of final observation point.
    if k > len(ksIdx):
        ksIdx.append(dataIdx.pop())
    
    return ksIdx