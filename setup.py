from setuptools import setup
import sys

# Python version enforcement.
if not sys.version_info[0] == 2:
    print "Python 3 is unsupported."
    exit()

setup(
    name='evoQSAR',
    version='1.0',
    packages=['evoqsar',],
    description='An evolutionary computation library for QSAR modeling',
    zip_safe=False, # Force unzipping of egg file. This is because PyInstaller cannot find the egg.
)