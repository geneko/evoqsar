# README #

This is the source code repository for the evoQSAR library, developed by Gene Ko for his [Ph.D. Dissertation in Computational Science at Claremont Graduate University and San Diego State University](https://sdsu-dspace.calstate.edu/handle/10211.3/142869).

### How do I get set up? ###

Anaconda packages of the evoQSAR library have been compiled.

To install evoQSAR using conda:
> conda install -c geneko evoqsar

To update evoQSAR using conda:
> conda update -c geneko evoqsar

## Dependencies ##
* Anaconda 2.5 (Python 2)